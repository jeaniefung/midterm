// Jeanie Fung jfung4
// Trisha Karani tkarani1
// ImageManip.h
// 601.220, Spring 2019

#ifndef BLUR_IMAGE_H
#define BLUR_IMAGE_H

#include <stdio.h>
#include "ppm_io.h"

#define PI 3.14159265

/*Functions created
*/
void bright (Image *image, int amnt_changed);

void gray (Image *image);

Image* createImage (int num_rows, int num_cols);

Image* crop (Image *image, int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y);

Image* blur (Image *image, double sigma);

void occlude (Image *image, int top_left_x, int top_left_y, int bottom_right_x, int bottom_right_y);

void edges (Image *image, int amnt_changed);

#endif
