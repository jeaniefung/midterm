// Jeanie Fung jfung4
// Trisha Karani tkarani1
// error_check.c
// 601.220, Spring 2019

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "ppm_io.h"
#include "imageManip.h" 
#include "error_check.h"

int check_params (Image *img, char* argv[], int argc, char funct) {
  for (int i = 4; i < argc; i++) {
    for(int j = 0; j < (int) strlen(argv[i]); j++) {
      printf("%c\n", argv[i][j]);
      if (argv[i][j] < 48 || argv[i][j] > 57) {
        fprintf(stderr, "Not a digit.\n");
        return 5;
      }
    }
  }
  switch (funct) {
    case 'r': //function is bright
      if (argc != 5) {
        fprintf(stderr, "Missing command arguments.\n");
        return 5;
      } break;
    case 'g': //function is gray
      if (argc != 4) {
        fprintf(stderr, "Missing command arguments.\n");
        return 5;
      } break;
    case 'c': //function is crop or occlude   
      if (argc != 8) {
        fprintf(stderr, "Missing command arguments.\n");
        return 5;
      } else if (atoi(argv[4]) < 0 || atoi(argv[5]) < 0 || //check if params are out of bounds
                 atoi(argv[6]) >= img->rows || atoi(argv[7]) >= img->cols ||
                 atoi(argv[4]) > atoi(argv[6]) || atoi(argv[5]) > atoi(argv[7])) {  //check if crop is not from top left to bottom right
        fprintf(stderr, "Invalid parameters.\n");
        return 1;
      } break; 
    case 'b': //fuction is blur
      if (argc != 5) {
        fprintf(stderr, "Missing command arguments.\n");
        return 5;
      } else if (atof(argv[4]) < 0.0) { //check if sigma is neg
        fprintf(stderr, "Invalid sigma value.\n");
        return 6;
      } break;
    case 'e': //function is edges
      if (argc != 6) {
        fprintf(stderr, "Missing command arguments.\n");
        return 5;
      }else if (atof(argv[4]) < 0.0 || atoi(argv[5]) < 0) { //check if sigma or threshod is neg
        fprintf(stderr, "Invalid values.\n");
          return 6;
      } break;
    } 
  return 0;
}

void close_file(Image *img, Image *edited_img) {
    free(img->data);
    free(img);
    free(edited_img);
}
