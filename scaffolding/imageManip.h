// Jeanie Fung jfung4
// Trisha Karani tkarani1
// ImageManip.h
// 601.220, Spring 2019

#ifndef IMAGEMANIP_H
#define IMAGEMANIP_H

#include <stdio.h>
#include "ppm_io.h"

//#define PI 3.14159265
#define PI 3.14

//Functions created to edit images

void bright (Image *image, Image* bright_img, int amnt_changed);

void gray (Image *image, Image* edited_img);

void crop (Image *image, Image* cropped_img, int top_x, int top_y, int bottom_x, int bottom_y);

void occlude (Image *image, Image* occ_img, int top_x, int top_y, int bottom_x, int bottom_y);

void blur (Image *image, Image* blurred_image, double sigma);

void edges (Image *image, Image* edged_image, double sigma, float threshold);

#endif
