// Jeanie Fung jfung4
// Trisha Karani tkarani1
// error_check.c
// 601.220, Spring 2019

#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <math.h>
#include "helper.h"

//HELPER METHODS FOR IMAGEMANIP:

//allocate space for new Image*
void allocate_image (Image* img, int num_rows, int num_cols) {
  img->rows = num_rows;
  img->cols = num_cols;
  //create space for pixels
  img->data = malloc(img->rows * img->cols * sizeof(Pixel*));

  if (img->data == NULL) {
    printf("Unable to allocate memory\n");
  }
}

//check to make sure results are within range. returns int
unsigned char int_to_char(int result) {
  unsigned char char_result = ' ';
  if (result < 0) {
    char_result = 0;
  }else if (result > 255) {
    char_result = 255;
  }else {
   char_result = (unsigned char)result;
  }
  return char_result; //return results
}

//check to make sure results are within range. returns unsigned char
unsigned char double_to_char(double result) {
  unsigned char char_result = ' ';
  if (result < 0) {
    char_result = 0;
  }else if (result > 255) {
    char_result = 255;
  }else {
   char_result = (unsigned char)result;
  }
  return char_result; //return results
}

//calculates intensity of pixel
unsigned char find_intensity(Pixel pixel) {
  int red = (int) pixel.r; 
  int green = (int) pixel.g; 
  int blue = (int) pixel.b; 

  //adjust intensity of each channel
  double intensity = 0.30*red + 0.59*green + 0.11*blue;

  return double_to_char(intensity); 
}


//create gaussian filter for edge and blur
double** create_gaussian(int dim, double sigma){
  int radius = dim / 2; 

  //allocate for 2D array
  double **g_matrix = malloc(sizeof(double)*dim); 
  for (int i = 0; i < dim; i++) {
	  g_matrix[i] = malloc(sizeof(double)*dim); 
  }

  double g_value = 0; //gaussian value
  int dx = 0;
  int dy = 0;

  //create gaussian matrix
  for (int y = 0; y < dim; y++){ //row
    for (int x=0; x < dim; x++){ //col
      dx = abs(radius - x); 
      dy = abs(radius - y); 
      g_value = (1.0 / (2.0 * PI * sigma * sigma)) * exp( -(dx * dx + dy * dy) / (2 * sigma * sigma)); 
      g_matrix[y][x] = g_value;
    }
  }
  return g_matrix; 
}	

//apply gaussian filter to pixel
Pixel blur_pixel(int p_y, int p_x, Image* image, double** gaussian, int dim) {
  double red_sum = 0.0; 
  double green_sum = 0.0; 
  double blue_sum = 0.0; 
  double g_sum = 0.0;
  int g_y = 0; 
  int radius = dim / 2;
  int image_loc = 0;

  //add rgb values from around center pixel 
  for (int y = -radius; y <= radius; y++,g_y++){ //rows
    for (int x = -radius, g_x=0; x <= radius; x++,g_x++) { //cols
      image_loc = ((p_y + y) * image->cols) + (p_x + x);
      //only use pixels within size of image
      if ((p_x + x <= (image->cols)-1) && (p_x + x >= 0) && 
          (p_y + y <= (image->rows)-1) && (p_y + y >= 0)) {
        red_sum += ((image->data)[image_loc].r) * gaussian[g_y][g_x];
        green_sum += ((image->data)[image_loc].g) * gaussian[g_y][g_x];
        blue_sum += ((image->data)[image_loc].b) * gaussian[g_y][g_x];
        g_sum += gaussian[g_y][g_x]; //add gaussian values used for normalization
      }
    }
  }
  //create new Pixel w/ normalized gaussian rgb
  Pixel p = {double_to_char(red_sum / g_sum), double_to_char(green_sum / g_sum), double_to_char(blue_sum / g_sum)};

  return p; 
}

//determine intensity gradients of pixels along X and Y axis
double pix_intensity (Image* edged_image, int p_y, int p_x) {
  double Vx = 0; //average of horizontal intensities
  double Vy = 0; //average of vertical intensities

  //determine horizontal and vertical intensity at interior point
  int cur_x = (p_y * edged_image->cols) + p_x;

  Vx = (edged_image->data)[cur_x + 1].r - (edged_image->data)[cur_x - 1].r;
  Vy = (edged_image->data)[((p_y + 1) * edged_image->cols) + p_x].r -
       (edged_image->data)[((p_y - 1) * edged_image->cols) + p_x].r;
  Vx = Vx/2;
  Vy = Vy/2;

  return sqrt(pow(Vx, 2.0) + pow(Vy, 2.0)); //return magnitude of intensity 
}

//HELPER METHODS FOR MAIN:
//check to make sure parameters from arguments are valid
int check_params (Image *img, char* argv[], int argc, char funct) {
  for (int i = 4; i < argc; i++) {
    for(int j = 0; j < (int) strlen(argv[i]); j++) {
      if (argv[i][j] == '-' && j!=0) {
      if ((argv[i][j] < 48 || argv[i][j] > 57) && argv[i][j] != '.') {
        fprintf(stderr, "Invalid digit.\n");
        return 5;
      }
      }
    }
  }
  switch (funct) {
    case 'r': //function is bright
      if (argc != 5) {
        fprintf(stderr, "Incorrect number of command arguments.\n");
        return 5;
      } break;
    case 'g': //function is gray
      if (argc != 4) {
        fprintf(stderr, "Incorrect number of command arguments.\n");
        return 5;
      } break;
    case 'c': //function is crop or edges   
      if (argc != 8) {
        fprintf(stderr, "Incorrect number of command arguments.\n");
        return 5;
      } else if (atoi(argv[4]) < 0 || atoi(argv[5]) < 0 || //check if params are out of bounds
                 atoi(argv[6]) >= img->rows || atoi(argv[7]) >= img->cols ||
                 atoi(argv[4]) > atoi(argv[6]) || atoi(argv[5]) > atoi(argv[7])) {  //check if crop is not from top left to bottom right
        fprintf(stderr, "Invalid parameters.\n");
        return 1;
      } break; 
    case 'b': //fuction is blur
      if (argc != 5) {
        fprintf(stderr, "Incorrect number of command arguments.\n");        
        return 5;
      } else if (atof(argv[4]) < 0.0) { //check if sigma is neg
        fprintf(stderr, "Incorrect number of command arguments.\n");
        return 6;
      } break;
    case 'e': //function is edges
      if (argc != 6) {
        fprintf(stderr, "Incorrect number of command arguments.\n");
        return 5;
      }else if (atof(argv[4]) < 0.0 || atof(argv[5]) < 0.0) { //check if sigma or threshod is neg
        fprintf(stderr, "Invalid input values.\n");
          return 6;
      } break;
    } 
  return 0; //no errors
}

//close files before ending run
void close_file(Image *img, Image *edited_img) {
    free(img->data);
    free(img);
    free(edited_img);
}
