// Jeanie Fung jfung4
// Trisha Karani tkarani1
// project.c
// 601.220, Spring 2019

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "ppm_io.h"
#include "imageManip.h"
#include "helper.h"

int main(int argc, char* argv[]) {
//check if at least 4 commands provided
  if (argc < 4) {
    fprintf(stderr,"Not enough arguments to perform any function.\n"); 
    return 1; 
  }

//check for input and output file names 
 if (strstr(argv[1], ".ppm") == NULL || strstr(argv[2], ".ppm") == NULL){
    fprintf(stderr, "Failed to supply input or output filename, or both.\n"); 
    return 1; 
 }

  //open image file and create Image
  Image* img;
  FILE* input = fopen(argv[1], "r");
      if (input == NULL) { //check if input file can be read
	 fclose(input);
	 fprintf(stderr, "Input file cannot be opened.\n");
	 return 2; 
      }
  img = read_ppm(input);  
     fclose(input);   
      if (img == NULL){ //check if input file is malformed
	 fprintf(stderr, "Input file is not a properly formatted PPM file or reading input has failed.\n"); 
	 return 3; 
      } 
  //image pointer to be used for imageManip funcs 
  Image* edited_img = (Image*) malloc(sizeof(Image));
  if (edited_img == NULL) {
    fprintf(stderr, "Unable to allocate memory\n");
    return 8;
  }
  
  //get imageManip func name 
  char* function = argv[3];
  int rtn = 0;

  //bright function
  if (strcmp(function, "bright") == 0) {
    rtn = check_params(img, argv, argc, 'r');
    if (rtn != 0) {
      close_file(img, edited_img);
      return rtn;
    } else {
	    bright(img, edited_img, atoi(argv[4]));
    }
  } //gray function
  else if (strcmp(function, "gray") == 0){
    rtn = check_params(img, argv, argc, 'g');
    if (rtn != 0) {
      close_file(img, edited_img);
      return rtn;
    } else {
      gray(img, edited_img); 
    }
  } //crop function
  else if (strcmp(function, "crop") == 0){
    rtn = check_params(img, argv, argc, 'c');
   
    if (rtn != 0) {
      close_file(img, edited_img);
      return rtn;
    } else {
      crop(img, edited_img, atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
    }
  }  //occlude function
  else if (strcmp(function, "occlude") == 0){
    rtn = check_params(img, argv, argc, 'c');
    if (rtn != 0) {
      close_file(img, edited_img);
      return rtn;
    } else {
      occlude(img, edited_img, atoi(argv[4]), atoi(argv[5]), atoi(argv[6]), atoi(argv[7]));
    }
  } //blur function
  else if (strcmp(function, "blur") == 0){
    rtn = check_params(img, argv, argc, 'b');
    if (rtn != 0) {
      close_file(img, edited_img);
      return rtn;
    } else {
      blur(img,edited_img, atof(argv[4]));
    }
  } //edges function
  else if (strcmp(function, "edges") == 0){
    rtn = check_params(img, argv, argc, 'e');
    if (rtn != 0) {
      close_file(img, edited_img);
      return rtn;
    } else {
      edges(img, edited_img,atof(argv[4]), atoi(argv[5]));
    }
  } //invalid
  else {
    fprintf(stderr, "Invalid operation name.\n");
    close_file(img, edited_img);
    return 4;
  } 

  
  //open file for writing
  FILE* output = fopen(argv[2], "w");
  int num_written = write_ppm(output, edited_img);  
 
 if((num_written != (img->cols * img->rows)) || ferror(output)){
     fprintf(stderr, "Unable to write to output file.\n");
     free(edited_img->data);
     close_file(img, edited_img);
     fclose(output); 
     return 7;
  } 
  
  //free allocated space
  free(edited_img->data);
  close_file(img, edited_img); 
  fclose(output); 

 return 0; 
}

