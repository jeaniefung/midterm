// Jeanie Fung jfung4
// Trisha Karani tkarani1
// ppm_io.c
// 601.220, Spring 2019

#include <assert.h>
#include "ppm_io.h"
#include<stdio.h>
#include<stdlib.h>

/* Read a PPM-formatted image from a file (assumes fp != NULL).
 * Returns the address of the heap-allocated Image struct it
 * creates and populates with the Image data.
 */
Image* read_ppm(FILE *fp) {
  // check that fp is not NULL
  assert(fp != NULL);

  Image* image = malloc(sizeof(Image));
  if (!image) {
         printf("Unable to allocate memory\n");
         //exit(1);
  }
  image->rows = 0;
  image->cols = 0;
  int numRead = 0;
  char p = ' ';
  int six = 0;
  int row = 0;
  int col = 0;

  //check first line for P6
  numRead = fscanf(fp, " %c%d", &p, &six);
  if ((numRead != 2) || (p != 'P') || (six != 6)){
     free(image);
     image = NULL;
     return image;
  }
  fgetc(fp); //buffer is new line
  //check for comments
  char c = fgetc(fp);
  char c_comment;
 if (c != '#' && (c<48 || c>57)){  //line after the first is not comment or number 
  free(image); 
  image = NULL; 
  return image; 
  }
  while (c == '#') {
    c_comment = '#';
    while (c_comment != '\n') { //keep reading next char
       c_comment = fgetc(fp);
    }
    if (c_comment == '\n') {
      c = fgetc(fp);
    }
  }
    ungetc(c, fp);

  //check for cols and rows

  numRead = fscanf(fp, "%d %d", &col, &row);
  if (numRead !=2){
     free(image);
     image = NULL;
     return image;
  }

  fgetc(fp); //buffer is new line
  image->rows = row;
  image->cols = col;

  //check for 255
  int max_color_value = 0;
  numRead = fscanf(fp, "%d", &max_color_value);
  if (numRead != 1 || max_color_value != 255) {
     free(image);
     image = NULL;
     return image;
  }

  fgetc(fp); //buffer is new line 

  //allocate space for data array
  image->data = malloc(image->cols * image->rows * sizeof(Pixel));

  //read pixels
  if((int)fread(image->data, sizeof(Pixel), image->cols * image->rows, fp) != (image->cols * image-> rows)){
     free(image->data); //error if num pixels expected != num pixels read
     free(image); 
     image = NULL;
     return image;

  } 

  //check if file is in error state 
  if(ferror(fp)){
     free(image->data);
     free(image);
     image = NULL;
     return image;
  }

  return image;
}

/* Write a PPM-formatted image to a file (assumes fp != NULL),
 * and return the number of pixels successfully written.
 */
int write_ppm(FILE *fp, const Image *im) {

  // check that fp is not NULL
  assert(fp);

  // write PPM file header, in the following format
  // P6
  // cols rows
  // 255
  fprintf(fp, "P6\n%d %d\n255\n", im->cols, im->rows);

  // now write the pixel array
  int num_pixels_written = fwrite(im->data, sizeof(Pixel), im->cols * im->rows, fp);

  if (num_pixels_written != im->cols * im->rows) {
    fprintf(stderr, "Uh oh. Pixel data failed to write properly!\n");
  }

  return num_pixels_written;
}




