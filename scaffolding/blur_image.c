//Jeanie Fung jfung4
//Trisha Karani tkarani1

#include <assert.h>
#include "blur_image.h"
#include<stdio.h>
#include<stdlib.h>
#include <math.h>

unsigned char results( int result) { 

  unsigned char char_result; 
  if (result < 0) {
    char_result = 0;  
  }
  else if (result > 255) {
    char_result = 255; 
  }
  else {
   char_result = (unsigned char)result; 
  }
  return char_result; 
}

unsigned char bright_sum(int amnt_changed, unsigned char color_value) {
  int new_color = (int) color_value; 
  int sum = new_color + amnt_changed; 

  return results(sum); 
}

void bright (Image *image, int amnt_changed) {
  int num_elements = (int) ((image->rows) * (image->cols));

  for (int i=0; i<num_elements; i++) {
    (image->data)[i].r = bright_sum(amnt_changed,(image->data)[i].r); 
    (image->data)[i].g = bright_sum(amnt_changed,(image->data)[i].g); 
    (image->data)[i].b = bright_sum(amnt_changed,(image->data)[i].b); 
  }
  

}

unsigned char find_intensity(Pixel pixel) {
  int red = (int) pixel.r; 
  int green = (int) pixel.g; 
  int blue = (int) pixel.b; 

  int intensity = 0.30*red + 0.59*green + 0.11*blue;

  return results(intensity); 
}

void gray (Image *image) {
  int num_elements = (int) ((image->rows) * (image->cols));
  unsigned char pixel_intensity; 
  
  for (int i = 0; i < num_elements; i++) {
    pixel_intensity = find_intensity((image->data)[i]); 
    (image->data)[i].r = pixel_intensity;
    (image->data)[i].g = pixel_intensity;
    (image->data)[i].b = pixel_intensity;
  } 
  
}

Image* createImage (int num_rows, int num_cols) {
  Image *img = (Image*) malloc(sizeof(Image));
  img->rows = num_rows;
  img->cols = num_cols;
  img->data = malloc(img->rows * img->cols * sizeof(Pixel*));

  return img;
}

Image* crop (Image *image, int top_x, int top_y, int bottom_x, int bottom_y) {
  printf("we're in\n");
  
  int num_cols = bottom_x - top_x + 1;
  int num_rows = bottom_y - top_y + 1;
  printf("cols: %d ", num_cols);
  printf("rows: %d\n", num_rows);
  int start = top_y * top_x;

  Image* img = createImage(num_rows, num_cols);
  printf("start: %d\n", start);

  for (int y = top_y; y < bottom_y; y++){ //rows
    for (int x = top_x; x < bottom_x; x++) { //cols
      (img->data)[((y * num_cols) + x) - ((top_y * num_cols) + top_x)] = (image->data) [(y * image->cols) + x];
    }
  }
  return img;
}

void occlude (Image *image, int top_x, int top_y, int bottom_x, int bottom_y) {
  for (int y = top_y; y < bottom_y; y++){ //rows
    for (int x = top_x; x < bottom_x; x++) { //cols
      (image->data) [(y * image->cols) + x].r = (unsigned char) 0;
      (image->data) [(y * image->cols) + x].g = (unsigned char) 0;
      (image->data) [(y * image->cols) + x].b = (unsigned char) 0;
    }
  }
 }

double** create_gaussian(int dim, double sigma){
  int radius = dim/2; 

  //allocate for 2D array
  double **g_matrix = malloc(sizeof(double)*dim); 
  for (int i=0; i<dim; i++) {
	  g_matrix[i] = malloc(sizeof(double)*dim); 
  }

  double g_value = 0;
  int dx = 0;
  int dy = 0;

  //create gaussian matrix
  for (int y=0; y<dim; y++){ //row
    for (int x=0; x<dim; x++) { //col
	dx = abs(radius - x); 
	dy = abs(radius - y); 
	
	g_value = (1.0 / (2.0 * PI * sigma * sigma)) * exp( -(dx*dx + dy*dy) / (2*sigma*sigma)); 

	g_matrix[y][x] = g_value;
    }
  }
  return g_matrix; 
}	


Pixel blur_pixel(int p_x, int p_y, Image* image, double** gaussian, int dim) {
  double red_sum = 0.0; 
  double green_sum = 0.0; 
  double blue_sum = 0.0; 
  int radius = dim/2;

  int g_y = 0;
  int image_loc = 0;
  double g_sum = 0.0; 

  //add rgb values from around center pixel 
  for (int y = -radius; y <= radius; y++,g_y++){ //rows
    for (int x = -radius, g_x=0; x <= radius; x++,g_x++) { //cols
       image_loc = ((p_y+y) * image->cols) + (p_x+x);
       
       //only use pixels within size of image
       if ((p_x + x <= (image->cols)-1) && (p_x + x >= 0) && 
          (p_y + y <= (image->rows)-1) && (p_y + y >= 0)){
        red_sum += ((image->data) [image_loc].r)*gaussian[g_y][g_x];
        green_sum += ((image->data) [image_loc].g)*gaussian[g_y][g_x];
        blue_sum += ((image->data) [image_loc].b)*gaussian[g_y][g_x];
       	g_sum += gaussian[g_y][g_x]; //add gaussian values used for normalization
      }
    }
  }
  
  //create new Pixel w/ normalized gaussian rgb
  Pixel p = {results(red_sum/g_sum), results(green_sum/g_sum), results(blue_sum/g_sum)};
  return p; 
  }

Image* blur (Image *image, double sigma) {
  
  //determine dimensions of gaussian 	
  int dim = sigma * 10;
  if (dim%2 ==0){ //if dim is even, increase by 1
     dim++;
  }

  //create gaussian matrix
  double** gaussian = create_gaussian(dim,sigma); 

  //create new instance of image 
  Image* blurred_image = malloc(sizeof(Image));
  if (!blurred_image) {
         printf("Unable to allocate memory\n");
         //exit(1);
  }

  //initialize row,col,data using original image
  blurred_image->cols = image->cols;
  blurred_image->rows = image->rows;
  blurred_image->data = malloc((blurred_image->cols) * (blurred_image->rows) * sizeof(Pixel));
  
  for (int y=0; y<image->rows; y++){
    for (int x=0; x<image->cols; x++) {
        blurred_image->data[(y*blurred_image->cols)+x] = blur_pixel(x,y,image,gaussian,dim); 
    }
  } 

  //free gaussian
  for (int i=0; i< dim; i++) {
    free(gaussian[i]);
  }
  free(gaussian);
 
  return blurred_image; 
}

 void edges (Image *image, int amnt_changed) {


 }
