// Jeanie Fung jfung4
// Trisha Karani tkarani1
// error_check.h
// 601.220, Spring 2019

#ifndef ERROR_CHECK_H
#define ERROR_CHECK_H

#include <stdio.h>
#include "ppm_io.h"
#include "imageManip.h"

//Helper methods for imageManip
unsigned char int_to_char(int result);

unsigned char double_to_char(double result);

unsigned char find_intensity(Pixel pixel);

void allocate_image (Image* img, int num_rows, int num_cols);

double** create_gaussian(int dim, double sigma);

Pixel blur_pixel(int p_y, int p_x, Image* image, double** gaussian, int dim);

double pix_intensity (Image* edged_image, int p_y, int p_x);

//Helper methods for main
int check_params (Image *img, char* argv[], int argc, char funct);

void close_file(Image *img, Image *edited_img);

#endif
