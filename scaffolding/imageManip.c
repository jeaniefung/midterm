// Jeanie Fung jfung4
// Trisha Karani tkarani1
// imageManip.c
// 601.220, Spring 2019

#include <assert.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>

#include "imageManip.h"
#include "helper.h"


//brighten image to specified value
void bright (Image* image, Image* bright_img, int amnt_changed){
  allocate_image(bright_img, image->rows, image->cols); //create space for new image
  int num_elements = (int)((image->rows) * (image->cols));
  
  //loop through image to adjust value
  for (int i = 0; i < num_elements; i++){
    (bright_img->data)[i].r = int_to_char(amnt_changed + (int)(image->data)[i].r); 
    (bright_img->data)[i].g = int_to_char(amnt_changed + (int)(image->data)[i].g); 
    (bright_img->data)[i].b = int_to_char(amnt_changed + (int)(image->data)[i].b); 
  }
}

//convert image to grayscale
void gray (Image* image, Image* gray_img) {
  
  allocate_image(gray_img, image->rows, image->cols); //create space for new image
  int num_elements = (int) ((image->rows) * (image->cols));
  unsigned char pixel_intensity = 0; 
  
  for (int i = 0; i < num_elements; i++){
    pixel_intensity = find_intensity((image->data)[i]); //adjusted intensity
    (gray_img->data)[i].r = pixel_intensity;
    (gray_img->data)[i].g = pixel_intensity;
    (gray_img->data)[i].b = pixel_intensity;
  } 
}

//crop image to specified dimensions
void crop (Image *image, Image* cropped_img, int top_x, int top_y, int bottom_x, int bottom_y){ 
  int num_cols = bottom_x - top_x; //cols of cropped image
  int num_rows = bottom_y - top_y; //rows of cropped image
  allocate_image(cropped_img,num_rows, num_cols); //create space for new image

  //copy pixels within specified parameter from original image to cropped array
  for (int y = top_y, c_y = 0; y < bottom_y; y++, c_y++){  //rows
    for (int x = top_x, c_x = 0; x < bottom_x; x++, c_x++){ //cols
      (cropped_img->data)[(c_y * num_cols) + c_x] = (image->data)[(y * image->cols) + x];
    }
  }
}

//black out image at specified area
void occlude (Image *image, Image* occ_img, int top_x, int top_y, int bottom_x, int bottom_y){
  allocate_image(occ_img, image->rows, image->cols); //create space for new image
  int loc = 0; //location of pixel
       
	//turn pixels within specified dememsions black
  for (int y = 0; y < occ_img->rows; y++){   //rows
    for (int x = 0; x < occ_img->cols; x++){ //cols
      loc = (y * image->cols) + x; 
     if (y>=top_y && y<= bottom_y && x>=top_x && x<=bottom_x){
      (occ_img->data) [loc].r = (unsigned char) 0;
      (occ_img->data) [loc].g = (unsigned char) 0;
      (occ_img->data) [loc].b = (unsigned char) 0;
     }else{
      (occ_img->data)[loc] = (image->data)[loc];   
     }
    }
  }
}

//blur image
void blur (Image *image, Image* blurred_image, double sigma){
  allocate_image(blurred_image, image->rows, image->cols); //create space for new image

  int dim = sigma * 10; //dimensions of gaussian 
  if (dim % 2 == 0) {   //make sure dim is odd
     dim++;
  }
  double** gaussian = create_gaussian(dim,sigma); //create gaussian matrix
  int loc = 0; //location of pixel

  //fill blurred_image with new blurred pixels
  for (int y = 0; y < image->rows; y++){
    for (int x = 0; x < image->cols; x++){
      loc = (y * blurred_image->cols) + x; 
      blurred_image->data[loc] = blur_pixel(y, x, image, gaussian, dim); 
    }
  } 
  //free gaussian
  for (int i=0; i< dim; i++){
    free(gaussian[i]);
  }
  free(gaussian);
}

//detect edges of image
void edges (Image *image, Image* edged_image, double sigma, float threshold){
  //create temp Image*
 printf("in edge\n"); 
      	Image *gray_image = (Image*) malloc(sizeof(Image));
  gray(image, gray_image); //convert to grayscale
printf("ran gray"); 
  Image *grayblur_image = (Image*) malloc(sizeof(Image));
  blur(gray_image, grayblur_image, sigma); //apply filter to reduce noise

  allocate_image(edged_image, image->rows, image->cols);

  //set the value of all channels to 0 (black) if the magnitude exceeds the threshold
  // else set the values to 255 (white)
  Pixel black = {0, 0, 0};
  Pixel white = {255, 255, 255};
  int loc = 0;
  for (int y = 0; y < edged_image->rows; y++){ //rows
    for (int x = 0; x < edged_image->cols; x++) { //cols
      loc = (y * image->cols) + x;

      if (y==0 || y==edged_image->rows-1 || x==0 || x==edged_image->cols-1) {
         (edged_image->data)[loc] = (grayblur_image->data)[loc];
      }

      else {

      if (pix_intensity(grayblur_image, y, x) > threshold) { //edge
        (edged_image->data)[loc]  = black;
      } else { //if (pix_intensity(edged_image, x, y) < threshold) { //not edge
        (edged_image->data)[loc]  = white;
      }

      }
    }
  }

  //free other image pointers 
  free(gray_image->data);
  free(gray_image);
  free(grayblur_image->data);
  free(grayblur_image);

}
